(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["crisis-center-crisis-center-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/crisis-center/crisis-center-home/crisis-center-home.component.html":
/*!**************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/crisis-center/crisis-center-home/crisis-center-home.component.html ***!
  \**************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<p>Welcome to the Crisis Center</p>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/crisis-center/crisis-center/crisis-center.component.html":
/*!****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/crisis-center/crisis-center/crisis-center.component.html ***!
  \****************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<h2>CRISIS CENTER</h2>\n<router-outlet></router-outlet>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/crisis-center/crisis-detail/crisis-detail.component.html":
/*!****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/crisis-center/crisis-detail/crisis-detail.component.html ***!
  \****************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div *ngIf=\"crisis\">\n  <h3>\"{{ editName }}\"</h3>\n  <div>\n    <label>Id: </label>{{ crisis.id }}</div>\n  <div>\n    <label>Name: </label>\n    <input [(ngModel)]=\"editName\" placeholder=\"name\"/>\n  </div>\n  <p>\n    <button (click)=\"save()\">Save</button>\n    <button (click)=\"cancel()\">Cancel</button>\n  </p>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/crisis-center/crisis-list/crisis-list.component.html":
/*!************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/crisis-center/crisis-list/crisis-list.component.html ***!
  \************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("  <ul class=\"crises\">\n    <li *ngFor=\"let crisis of crises$ | async\"\n      [class.selected]=\"crisis.id === selectedId\">\n      <a [routerLink]=\"[crisis.id]\">\n        <span class=\"badge\">{{ crisis.id }}</span>{{ crisis.name }}\n      </a>\n    </li>\n  </ul>\n\n  <router-outlet></router-outlet>");

/***/ }),

/***/ "./src/app/can-deactivate.guard.ts":
/*!*****************************************!*\
  !*** ./src/app/can-deactivate.guard.ts ***!
  \*****************************************/
/*! exports provided: CanDeactivateGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CanDeactivateGuard", function() { return CanDeactivateGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


let CanDeactivateGuard = class CanDeactivateGuard {
    canDeactivate(component) {
        return component.canDeactivate ? component.canDeactivate() : true;
    }
};
CanDeactivateGuard = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root',
    })
], CanDeactivateGuard);



/***/ }),

/***/ "./src/app/crisis-center/crisis-center-home/crisis-center-home.component.css":
/*!***********************************************************************************!*\
  !*** ./src/app/crisis-center/crisis-center-home/crisis-center-home.component.css ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJjcmlzaXMtY2VudGVyL2NyaXNpcy1jZW50ZXItaG9tZS9jcmlzaXMtY2VudGVyLWhvbWUuY29tcG9uZW50LmNzcyJ9 */");

/***/ }),

/***/ "./src/app/crisis-center/crisis-center-home/crisis-center-home.component.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/crisis-center/crisis-center-home/crisis-center-home.component.ts ***!
  \**********************************************************************************/
/*! exports provided: CrisisCenterHomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CrisisCenterHomeComponent", function() { return CrisisCenterHomeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


let CrisisCenterHomeComponent = class CrisisCenterHomeComponent {
};
CrisisCenterHomeComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-crisis-center-home',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./crisis-center-home.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/crisis-center/crisis-center-home/crisis-center-home.component.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./crisis-center-home.component.css */ "./src/app/crisis-center/crisis-center-home/crisis-center-home.component.css")).default]
    })
], CrisisCenterHomeComponent);



/***/ }),

/***/ "./src/app/crisis-center/crisis-center-routing.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/crisis-center/crisis-center-routing.module.ts ***!
  \***************************************************************/
/*! exports provided: CrisisCenterRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CrisisCenterRoutingModule", function() { return CrisisCenterRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _crisis_center_home_crisis_center_home_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./crisis-center-home/crisis-center-home.component */ "./src/app/crisis-center/crisis-center-home/crisis-center-home.component.ts");
/* harmony import */ var _crisis_list_crisis_list_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./crisis-list/crisis-list.component */ "./src/app/crisis-center/crisis-list/crisis-list.component.ts");
/* harmony import */ var _crisis_center_crisis_center_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./crisis-center/crisis-center.component */ "./src/app/crisis-center/crisis-center/crisis-center.component.ts");
/* harmony import */ var _crisis_detail_crisis_detail_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./crisis-detail/crisis-detail.component */ "./src/app/crisis-center/crisis-detail/crisis-detail.component.ts");
/* harmony import */ var _can_deactivate_guard__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../can-deactivate.guard */ "./src/app/can-deactivate.guard.ts");
/* harmony import */ var _crisis_detail_resolver_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./crisis-detail-resolver.service */ "./src/app/crisis-center/crisis-detail-resolver.service.ts");









const crisisCenterRoutes = [
    {
        path: '',
        component: _crisis_center_crisis_center_component__WEBPACK_IMPORTED_MODULE_5__["CrisisCenterComponent"],
        children: [
            {
                path: '',
                component: _crisis_list_crisis_list_component__WEBPACK_IMPORTED_MODULE_4__["CrisisListComponent"],
                children: [
                    {
                        path: ':id',
                        component: _crisis_detail_crisis_detail_component__WEBPACK_IMPORTED_MODULE_6__["CrisisDetailComponent"],
                        canDeactivate: [_can_deactivate_guard__WEBPACK_IMPORTED_MODULE_7__["CanDeactivateGuard"]],
                        resolve: {
                            crisis: _crisis_detail_resolver_service__WEBPACK_IMPORTED_MODULE_8__["CrisisDetailResolverService"]
                        }
                    },
                    {
                        path: '',
                        component: _crisis_center_home_crisis_center_home_component__WEBPACK_IMPORTED_MODULE_3__["CrisisCenterHomeComponent"]
                    }
                ]
            }
        ]
    }
];
let CrisisCenterRoutingModule = class CrisisCenterRoutingModule {
};
CrisisCenterRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(crisisCenterRoutes)
        ],
        exports: [
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]
        ]
    })
], CrisisCenterRoutingModule);



/***/ }),

/***/ "./src/app/crisis-center/crisis-center.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/crisis-center/crisis-center.module.ts ***!
  \*******************************************************/
/*! exports provided: CrisisCenterModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CrisisCenterModule", function() { return CrisisCenterModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _crisis_center_home_crisis_center_home_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./crisis-center-home/crisis-center-home.component */ "./src/app/crisis-center/crisis-center-home/crisis-center-home.component.ts");
/* harmony import */ var _crisis_list_crisis_list_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./crisis-list/crisis-list.component */ "./src/app/crisis-center/crisis-list/crisis-list.component.ts");
/* harmony import */ var _crisis_center_crisis_center_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./crisis-center/crisis-center.component */ "./src/app/crisis-center/crisis-center/crisis-center.component.ts");
/* harmony import */ var _crisis_detail_crisis_detail_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./crisis-detail/crisis-detail.component */ "./src/app/crisis-center/crisis-detail/crisis-detail.component.ts");
/* harmony import */ var _crisis_center_routing_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./crisis-center-routing.module */ "./src/app/crisis-center/crisis-center-routing.module.ts");









let CrisisCenterModule = class CrisisCenterModule {
};
CrisisCenterModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
            _crisis_center_routing_module__WEBPACK_IMPORTED_MODULE_8__["CrisisCenterRoutingModule"]
        ],
        declarations: [
            _crisis_center_crisis_center_component__WEBPACK_IMPORTED_MODULE_6__["CrisisCenterComponent"],
            _crisis_list_crisis_list_component__WEBPACK_IMPORTED_MODULE_5__["CrisisListComponent"],
            _crisis_center_home_crisis_center_home_component__WEBPACK_IMPORTED_MODULE_4__["CrisisCenterHomeComponent"],
            _crisis_detail_crisis_detail_component__WEBPACK_IMPORTED_MODULE_7__["CrisisDetailComponent"]
        ]
    })
], CrisisCenterModule);



/***/ }),

/***/ "./src/app/crisis-center/crisis-center/crisis-center.component.css":
/*!*************************************************************************!*\
  !*** ./src/app/crisis-center/crisis-center/crisis-center.component.css ***!
  \*************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJjcmlzaXMtY2VudGVyL2NyaXNpcy1jZW50ZXIvY3Jpc2lzLWNlbnRlci5jb21wb25lbnQuY3NzIn0= */");

/***/ }),

/***/ "./src/app/crisis-center/crisis-center/crisis-center.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/crisis-center/crisis-center/crisis-center.component.ts ***!
  \************************************************************************/
/*! exports provided: CrisisCenterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CrisisCenterComponent", function() { return CrisisCenterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


let CrisisCenterComponent = class CrisisCenterComponent {
};
CrisisCenterComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-crisis-center',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./crisis-center.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/crisis-center/crisis-center/crisis-center.component.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./crisis-center.component.css */ "./src/app/crisis-center/crisis-center/crisis-center.component.css")).default]
    })
], CrisisCenterComponent);



/***/ }),

/***/ "./src/app/crisis-center/crisis-detail-resolver.service.ts":
/*!*****************************************************************!*\
  !*** ./src/app/crisis-center/crisis-detail-resolver.service.ts ***!
  \*****************************************************************/
/*! exports provided: CrisisDetailResolverService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CrisisDetailResolverService", function() { return CrisisDetailResolverService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _crisis_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./crisis.service */ "./src/app/crisis-center/crisis.service.ts");






let CrisisDetailResolverService = class CrisisDetailResolverService {
    constructor(cs, router) {
        this.cs = cs;
        this.router = router;
    }
    resolve(route, state) {
        let id = route.paramMap.get('id');
        return this.cs.getCrisis(id).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["take"])(1), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["mergeMap"])(crisis => {
            if (crisis) {
                return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(crisis);
            }
            else { // id not found
                this.router.navigate(['/crisis-center']);
                return rxjs__WEBPACK_IMPORTED_MODULE_3__["EMPTY"];
            }
        }));
    }
};
CrisisDetailResolverService.ctorParameters = () => [
    { type: _crisis_service__WEBPACK_IMPORTED_MODULE_5__["CrisisService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
CrisisDetailResolverService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root',
    })
], CrisisDetailResolverService);



/***/ }),

/***/ "./src/app/crisis-center/crisis-detail/crisis-detail.component.css":
/*!*************************************************************************!*\
  !*** ./src/app/crisis-center/crisis-detail/crisis-detail.component.css ***!
  \*************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("input {\n  width: 20em\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNyaXNpcy1jZW50ZXIvY3Jpc2lzLWRldGFpbC9jcmlzaXMtZGV0YWlsLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRTtBQUNGIiwiZmlsZSI6ImNyaXNpcy1jZW50ZXIvY3Jpc2lzLWRldGFpbC9jcmlzaXMtZGV0YWlsLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpbnB1dCB7XG4gIHdpZHRoOiAyMGVtXG59Il19 */");

/***/ }),

/***/ "./src/app/crisis-center/crisis-detail/crisis-detail.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/crisis-center/crisis-detail/crisis-detail.component.ts ***!
  \************************************************************************/
/*! exports provided: CrisisDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CrisisDetailComponent", function() { return CrisisDetailComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _dialog_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../dialog.service */ "./src/app/dialog.service.ts");




let CrisisDetailComponent = class CrisisDetailComponent {
    constructor(route, router, dialogService) {
        this.route = route;
        this.router = router;
        this.dialogService = dialogService;
    }
    ngOnInit() {
        this.route.data
            .subscribe((data) => {
            this.editName = data.crisis.name;
            this.crisis = data.crisis;
        });
    }
    cancel() {
        this.gotoCrises();
    }
    save() {
        this.crisis.name = this.editName;
        this.gotoCrises();
    }
    canDeactivate() {
        // Allow synchronous navigation (`true`) if no crisis or the crisis is unchanged
        if (!this.crisis || this.crisis.name === this.editName) {
            return true;
        }
        // Otherwise ask the user with the dialog service and return its
        // observable which resolves to true or false when the user decides
        return this.dialogService.confirm('Discard changes?');
    }
    gotoCrises() {
        let crisisId = this.crisis ? this.crisis.id : null;
        // Pass along the crisis id if available
        // so that the CrisisListComponent can select that crisis.
        // Add a totally useless `foo` parameter for kicks.
        // Relative navigation back to the crises
        this.router.navigate(['../', { id: crisisId, foo: 'foo' }], { relativeTo: this.route });
    }
};
CrisisDetailComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _dialog_service__WEBPACK_IMPORTED_MODULE_3__["DialogService"] }
];
CrisisDetailComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-crisis-detail',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./crisis-detail.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/crisis-center/crisis-detail/crisis-detail.component.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./crisis-detail.component.css */ "./src/app/crisis-center/crisis-detail/crisis-detail.component.css")).default]
    })
], CrisisDetailComponent);



/***/ }),

/***/ "./src/app/crisis-center/crisis-list/crisis-list.component.css":
/*!*********************************************************************!*\
  !*** ./src/app/crisis-center/crisis-list/crisis-list.component.css ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("/* CrisisListComponent's private CSS styles */\n.crises {\n  margin: 0 0 2em 0;\n  list-style-type: none;\n  padding: 0;\n  width: 24em;\n}\n.crises li {\n  position: relative;\n  cursor: pointer;\n  background-color: #EEE;\n  margin: .5em;\n  padding: .3em 0;\n  height: 1.6em;\n  border-radius: 4px;\n}\n.crises li:hover {\n  color: #607D8B;\n  background-color: #DDD;\n  left: .1em;\n}\n.crises a {\n  color: #888;\n  text-decoration: none;\n  display: block;\n}\n.crises a:hover {\n  color:#607D8B;\n}\n.crises .badge {\n  display: inline-block;\n  font-size: small;\n  color: white;\n  padding: 0.8em 0.7em 0 0.7em;\n  background-color: #607D8B;\n  line-height: 1em;\n  position: relative;\n  left: -1px;\n  top: -4px;\n  height: 1.8em;\n  min-width: 16px;\n  text-align: right;\n  margin-right: .8em;\n  border-radius: 4px 0 0 4px;\n}\nbutton {\n  background-color: #eee;\n  border: none;\n  padding: 5px 10px;\n  border-radius: 4px;\n  cursor: pointer;\n  cursor: hand;\n  font-family: Arial;\n}\nbutton:hover {\n  background-color: #cfd8dc;\n}\nbutton.delete {\n  position: relative;\n  left: 194px;\n  top: -32px;\n  background-color: gray !important;\n  color: white;\n}\n.crises li.selected {\n  background-color: #CFD8DC;\n  color: white;\n}\n.crises li.selected:hover {\n  background-color: #BBD8DC;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNyaXNpcy1jZW50ZXIvY3Jpc2lzLWxpc3QvY3Jpc2lzLWxpc3QuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSw2Q0FBNkM7QUFDN0M7RUFDRSxpQkFBaUI7RUFDakIscUJBQXFCO0VBQ3JCLFVBQVU7RUFDVixXQUFXO0FBQ2I7QUFDQTtFQUNFLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2Ysc0JBQXNCO0VBQ3RCLFlBQVk7RUFDWixlQUFlO0VBQ2YsYUFBYTtFQUNiLGtCQUFrQjtBQUNwQjtBQUVBO0VBQ0UsY0FBYztFQUNkLHNCQUFzQjtFQUN0QixVQUFVO0FBQ1o7QUFFQTtFQUNFLFdBQVc7RUFDWCxxQkFBcUI7RUFDckIsY0FBYztBQUNoQjtBQUVBO0VBQ0UsYUFBYTtBQUNmO0FBRUE7RUFDRSxxQkFBcUI7RUFDckIsZ0JBQWdCO0VBQ2hCLFlBQVk7RUFDWiw0QkFBNEI7RUFDNUIseUJBQXlCO0VBQ3pCLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsVUFBVTtFQUNWLFNBQVM7RUFDVCxhQUFhO0VBQ2IsZUFBZTtFQUNmLGlCQUFpQjtFQUNqQixrQkFBa0I7RUFDbEIsMEJBQTBCO0FBQzVCO0FBRUE7RUFDRSxzQkFBc0I7RUFDdEIsWUFBWTtFQUNaLGlCQUFpQjtFQUNqQixrQkFBa0I7RUFDbEIsZUFBZTtFQUNmLFlBQVk7RUFDWixrQkFBa0I7QUFDcEI7QUFFQTtFQUNFLHlCQUF5QjtBQUMzQjtBQUVBO0VBQ0Usa0JBQWtCO0VBQ2xCLFdBQVc7RUFDWCxVQUFVO0VBQ1YsaUNBQWlDO0VBQ2pDLFlBQVk7QUFDZDtBQUVBO0VBQ0UseUJBQXlCO0VBQ3pCLFlBQVk7QUFDZDtBQUNBO0VBQ0UseUJBQXlCO0FBQzNCIiwiZmlsZSI6ImNyaXNpcy1jZW50ZXIvY3Jpc2lzLWxpc3QvY3Jpc2lzLWxpc3QuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIENyaXNpc0xpc3RDb21wb25lbnQncyBwcml2YXRlIENTUyBzdHlsZXMgKi9cbi5jcmlzZXMge1xuICBtYXJnaW46IDAgMCAyZW0gMDtcbiAgbGlzdC1zdHlsZS10eXBlOiBub25lO1xuICBwYWRkaW5nOiAwO1xuICB3aWR0aDogMjRlbTtcbn1cbi5jcmlzZXMgbGkge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGN1cnNvcjogcG9pbnRlcjtcbiAgYmFja2dyb3VuZC1jb2xvcjogI0VFRTtcbiAgbWFyZ2luOiAuNWVtO1xuICBwYWRkaW5nOiAuM2VtIDA7XG4gIGhlaWdodDogMS42ZW07XG4gIGJvcmRlci1yYWRpdXM6IDRweDtcbn1cblxuLmNyaXNlcyBsaTpob3ZlciB7XG4gIGNvbG9yOiAjNjA3RDhCO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjREREO1xuICBsZWZ0OiAuMWVtO1xufVxuXG4uY3Jpc2VzIGEge1xuICBjb2xvcjogIzg4ODtcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICBkaXNwbGF5OiBibG9jaztcbn1cblxuLmNyaXNlcyBhOmhvdmVyIHtcbiAgY29sb3I6IzYwN0Q4Qjtcbn1cblxuLmNyaXNlcyAuYmFkZ2Uge1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIGZvbnQtc2l6ZTogc21hbGw7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgcGFkZGluZzogMC44ZW0gMC43ZW0gMCAwLjdlbTtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzYwN0Q4QjtcbiAgbGluZS1oZWlnaHQ6IDFlbTtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBsZWZ0OiAtMXB4O1xuICB0b3A6IC00cHg7XG4gIGhlaWdodDogMS44ZW07XG4gIG1pbi13aWR0aDogMTZweDtcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gIG1hcmdpbi1yaWdodDogLjhlbTtcbiAgYm9yZGVyLXJhZGl1czogNHB4IDAgMCA0cHg7XG59XG5cbmJ1dHRvbiB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNlZWU7XG4gIGJvcmRlcjogbm9uZTtcbiAgcGFkZGluZzogNXB4IDEwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgY3Vyc29yOiBwb2ludGVyO1xuICBjdXJzb3I6IGhhbmQ7XG4gIGZvbnQtZmFtaWx5OiBBcmlhbDtcbn1cblxuYnV0dG9uOmhvdmVyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2NmZDhkYztcbn1cblxuYnV0dG9uLmRlbGV0ZSB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgbGVmdDogMTk0cHg7XG4gIHRvcDogLTMycHg7XG4gIGJhY2tncm91bmQtY29sb3I6IGdyYXkgIWltcG9ydGFudDtcbiAgY29sb3I6IHdoaXRlO1xufVxuXG4uY3Jpc2VzIGxpLnNlbGVjdGVkIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI0NGRDhEQztcbiAgY29sb3I6IHdoaXRlO1xufVxuLmNyaXNlcyBsaS5zZWxlY3RlZDpob3ZlciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNCQkQ4REM7XG59Il19 */");

/***/ }),

/***/ "./src/app/crisis-center/crisis-list/crisis-list.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/crisis-center/crisis-list/crisis-list.component.ts ***!
  \********************************************************************/
/*! exports provided: CrisisListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CrisisListComponent", function() { return CrisisListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _crisis_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../crisis.service */ "./src/app/crisis-center/crisis.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");





let CrisisListComponent = class CrisisListComponent {
    constructor(service, route) {
        this.service = service;
        this.route = route;
    }
    ngOnInit() {
        this.crises$ = this.route.paramMap.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["switchMap"])(params => {
            this.selectedId = +params.get('id');
            return this.service.getCrises();
        }));
    }
};
CrisisListComponent.ctorParameters = () => [
    { type: _crisis_service__WEBPACK_IMPORTED_MODULE_3__["CrisisService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] }
];
CrisisListComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-crisis-list',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./crisis-list.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/crisis-center/crisis-list/crisis-list.component.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./crisis-list.component.css */ "./src/app/crisis-center/crisis-list/crisis-list.component.css")).default]
    })
], CrisisListComponent);



/***/ }),

/***/ "./src/app/crisis-center/crisis.service.ts":
/*!*************************************************!*\
  !*** ./src/app/crisis-center/crisis.service.ts ***!
  \*************************************************/
/*! exports provided: CrisisService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CrisisService", function() { return CrisisService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _message_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../message.service */ "./src/app/message.service.ts");
/* harmony import */ var _mock_crises__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./mock-crises */ "./src/app/crisis-center/mock-crises.ts");
var CrisisService_1;






let CrisisService = CrisisService_1 = class CrisisService {
    constructor(messageService) {
        this.messageService = messageService;
        this.crises$ = new rxjs__WEBPACK_IMPORTED_MODULE_1__["BehaviorSubject"](_mock_crises__WEBPACK_IMPORTED_MODULE_5__["CRISES"]);
    }
    getCrises() { return this.crises$; }
    getCrisis(id) {
        return this.getCrises().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(crises => crises.find(crisis => crisis.id === +id)));
    }
    addCrisis(name) {
        name = name.trim();
        if (name) {
            let crisis = { id: CrisisService_1.nextCrisisId++, name };
            _mock_crises__WEBPACK_IMPORTED_MODULE_5__["CRISES"].push(crisis);
            this.crises$.next(_mock_crises__WEBPACK_IMPORTED_MODULE_5__["CRISES"]);
        }
    }
};
CrisisService.nextCrisisId = 100;
CrisisService.ctorParameters = () => [
    { type: _message_service__WEBPACK_IMPORTED_MODULE_4__["MessageService"] }
];
CrisisService = CrisisService_1 = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Injectable"])({
        providedIn: 'root',
    })
], CrisisService);



/***/ }),

/***/ "./src/app/crisis-center/mock-crises.ts":
/*!**********************************************!*\
  !*** ./src/app/crisis-center/mock-crises.ts ***!
  \**********************************************/
/*! exports provided: CRISES */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CRISES", function() { return CRISES; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");

const CRISES = [
    { id: 1, name: 'Dragon Burning Cities' },
    { id: 2, name: 'Sky Rains Great White Sharks' },
    { id: 3, name: 'Giant Asteroid Heading For Earth' },
    { id: 4, name: 'Procrastinators Meeting Delayed Again' },
];


/***/ }),

/***/ "./src/app/dialog.service.ts":
/*!***********************************!*\
  !*** ./src/app/dialog.service.ts ***!
  \***********************************/
/*! exports provided: DialogService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DialogService", function() { return DialogService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");



/**
 * Async modal dialog service
 * DialogService makes this app easier to test by faking this service.
 * TODO: better modal implementation that doesn't use window.confirm
 */
let DialogService = class DialogService {
    /**
     * Ask user to confirm an action. `message` explains the action and choices.
     * Returns observable resolving to `true`=confirm or `false`=cancel
     */
    confirm(message) {
        const confirmation = window.confirm(message || 'Is it OK?');
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])(confirmation);
    }
    ;
};
DialogService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root',
    })
], DialogService);



/***/ })

}]);
//# sourceMappingURL=crisis-center-crisis-center-module-es2015.js.map